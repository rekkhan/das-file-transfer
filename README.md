# Overview
This simple script, `dastf.sh`, allows transferring official datasets to local machine.

The `dasgoclient` command is used to grab the list of the root files in the dataset. The files are then downloaded by `gfal-copy`.


# Requirements
Access to the remote server:
- You need cern certificate to setup a proxy to connect to the remote server.

The `dasgoclient` tool:
- CernVM file-system (cvmfs) is mounted, `dasgoclient` is available to use. (the tool is at `/cvmfs/cms.cern.ch/common/dasgoclient` )
- You can check if the tool is ready by:
```bash
$ which dasgoclient
```

The `gfal-copy`
- The `gfal2` package is available on chip02.


### Request grid certificate
Please try to follow the steps in [this page](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookStartingGrid#ObtainingCert)

tl;dr:
- Get a grid certificate on [CERN CA page](https://ca.cern.ch/ca/). You'll receive a file with extension `p12` (E.g. myCert.p12)
- Create the `.globus` directory in your home directory if it does not exist
```bash
$ mkdir ~/.globus
```
- Copy your `myCert.p12` to the `.globus` folder
- Execute the following commands
```bash
$ cd ~/.globus
$ openssl pkcs12 -in mycert.p12 -clcerts -nokeys -out usercert.pem
$ openssl pkcs12 -in mycert.p12 -nocerts -out userkey.pem
$ chmod 400 userkey.pem
$ chmod 400 usercert.pem
```


### Set up the proxy
After the certificate is set up, run the following command on your terminal to setup the proxy
```bash
$ voms-proxy-init --voms cms --valid 168:00
```
you are prompted to enter the password. If the steps are done correctly, then you will see the message:
```
Contacting voms2.cern.ch:XXX [...] "cms"...
Remote VOMS server contacted succesfully.

WARNING: proxy lifetime limited to issuing credential lifetime.

Created proxy in <...>

Your proxy is valid until <the date the proxy expires>
```


# Usage
### Preparation
1. As only a single script is required to do the task, simply download `dastf.sh` to your computer.
2. Give the script permission to execute:
```
$ chmod +x dastf.sh
```
3. Setup the proxy as mentioned above if you didn't.


### Syntax
To download a single dataset
```bash
$ ./dastf.sh [options] redirector dataset_name target_directory number_of_parallel_tasks (optional)log_description
```

To download multiple datasets
```bash
$ ./dastf.sh -i [options] path_to_indicator number_of_parallel_tasks (optional)log_description
```

Options:
- `-f`: re-download the missing files

Log file:
- A log keeping track of the process is store in `Log_Transfer` directory. If the log_description is empty, then the log name is the date&time the script starts by default. You can change the value of `log_description` to change the log name.


### The indicator file
An indicator is a list of datasets (following the DAS format) and the corresponding directories where the file will be downloaded to.

The format of an indicator
```
redirector: root://name-of-redirector

dataset: /name/of/dataset1
target:  /local/directory1/

dataset: /name/of/dataset2
target:  /local/directory2/

...
```

example:
```
redirector: root://xrootd-cms.infn.it

dataset: /Muon/Run2022G-PromptNanoAODv11_v1-v2/NANOAOD
target: /data4/hoa/public/RemoteTransfer/TestDestination2
```



### Redirector
Available redirector according to the [twiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookXrootdService#ReDirector)
```
For people in the US:       cmsxrootd.fnal.gov
For people in Europe/Asia:  xrootd-cms.infn.it
Global redirector:          cms-xrd-global.cern.ch
```



# Changelog
Update on 2023-07-02
- Upload the script



# TO-DO list
 - [ ] find a better way to store the individual logs of each transferred file.
