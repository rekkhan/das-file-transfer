#!/bin/bash


# Parallel jobs monitor
function maxbg
{
	nmaxbg=$1
	ncurbg=$( jobs -pr | wc -w )
	while [ $ncurbg -ge $nmaxbg ]; do
		sleep 1
		ncurbg=$( jobs -pr | wc -w )
	done
}


# Lists
file_list=( )
redirector=( )
source_list=( )
target_list=( )

# Obtain the list of sources and targets from indicator file
function list_from_indicator
{
	source_list=( )
	target_list=( )
	local indicator_file=$1
	while read -r line; do
		if [[ "$line" =~ [[:blank:]]*redirector.* ]]; then
			redirector="$( echo $line | awk '{print $2}' )"
		fi
		if [[ "$line" =~ [[:blank:]]*dataset.* ]]; then
			local dataset="$( echo $line | awk '{print $2}' )"
			source_list+=( "$dataset" )
			continue
		fi
		if [[ "$line" =~ [[:blank:]]*target.* ]]; then
			local target_dir="$( echo $line | awk '{print $2}' )"
			target_list+=( "$target_dir" )
			continue
		fi
	done < "$indicator_file"
}

# Obtain the source and target from cmd line args
function list_from_argument
{
	redirector="$1"
	source_list+=( "$2" )
	target_list+=( "$3" )
}

# Obtain the list of files
function acquire_file_list
{
	local dataset=$1
	local target_dir=$2
	if [ $cp_missing -eq 0 ]; then
		file_list=( $( dasgoclient --query "file dataset=$dataset" ) )
		return
	fi
	local tmp_sources=( $( dasgoclient --query "file dataset=$dataset" ) )
	local tmp_targets=( $( ls "$target_dir" ) )
	for src in ${tmp_sources[@]}; do
		match=0
		for tar in ${tmp_targets[@]}; do
			if ! [[ "$src" =~ .*${tar} ]]; then continue; fi
			match=1 && break;
		done
		if [ $match -eq 0 ]; then file_list+=( "$src" ); fi
	done
}

# Transfer file
function remote_transfer
{
	local dir_id=$1
	local file_id=$2
	local file_path="$redirector/${file_list[file_id]}"
	local destination="${target_list[dir_id]}"
	{ gfal-copy $file_path $destination 1> /dev/null; } && echo "done"
}

# File transfer monitor ( support parallel jobs )
function job_monitor
{
	#number of parallel jobs
	local n_jobs=$1
	local usr_title="$2"
	echo "required [$n_jobs] jobs"

	local date_text="$(date)"
	local date_text="${date_text//" "/_}"
	local date_text="${date_text//":"/_}"
	local title="$( [[ $usr_title == "" ]] && echo "$date_text" || echo "$usr_title" )"
	log_dir="Log_Transfer"
	if [ $cp_missing -eq 1 ]; then log_dir="Log_Fix"; fi

	for srcid in ${!source_list[@]}; do
		# give the log file name a date
		mkdir -p $log_dir
		local log_pattern="$( printf "transfer_%s_%02d" $title $srcid )"
		local step_log_dir="$log_dir/${log_pattern^^}"
		mkdir -p "$step_log_dir"

		# get the list of files to transfer
		echo "source: ${source_list[srcid]}"
		file_list=( )
		acquire_file_list "${source_list[srcid]}" "${target_list[srcid]}"

		# transfer file
		local time1=$( date +%s )
		mkdir -p "${target_list[srcid]}"
		for fid in ${!file_list[@]}; do
			local step_log="$( printf "${step_log_dir}/step%05d.log" $fid )"
			remote_transfer $srcid $fid &> "$step_log" &
			maxbg $n_jobs
		done
		wait
		local time2=$( date +%s )
		local tftime=$((time2-time1))

		# print status
		local count_success=0
		local full_log="$log_dir/${log_pattern}.log"
		echo -e "destination: ${target_list[srcid]}\n" > "$full_log"
		for fid in ${!file_list[@]}; do
			local step_log="$( printf "${step_log_dir}/step%05d.log" $fid )"
			local message=""
			if [[ -f $step_log ]]; then message="$( cat $step_log )"; fi
			if [[ "$message" == "" ]]; then
				message="unknown error, remove target"
				local mark_for_rm="${target_list[srcid]}/${file_list[fid]}"
				if [[ -f "$mark_for_rm" ]]; then rm "$mark_for_rm"; fi
			fi
			if [[ "$message" == "done" ]]; then count_success=$((count_success+1)); fi
			echo "[remote file] ${file_list[fid]}" >> "$full_log"
			echo "[message] $message" >> "$full_log"
			echo >> "$full_log"
		done
		if [[ -d "$step_log_dir" ]]; then rm -r "$step_log_dir"; fi
		local tfsize=$( du -s --block-size=1KB ${target_list[srcid]} | grep -o ^[0-9]* )
		local speed=$(( tfsize/tftime ))
		echo "sucessfully copied [$count_success] files" >> "$full_log"
		echo "time: $tftime (s); size: $tfsize (KB); speed: $speed (KB/s)" >> "$full_log"
	done
}

opts=""
args=( )
for arg in $@; do
	if [[ ${arg:0:1} != '-' ]]; then args+=( "$arg" );
	else opts+="${arg/-/}"; fi
done

use_indicator=0
cp_missing=0
for ch in $( seq 1 ${#opts} ); do
	case ${opts:ch-1:1} in
		i) use_indicator=1;;
		f) cp_missing=1;;
	esac
done
if [ $use_indicator -eq 0 ]; then
	list_from_argument "${args[0]}" "${args[1]}" "${args[2]}"
	job_monitor "${args[3]}" "${args[4]}"
	exit
else
	list_from_indicator "${args[0]}"
	job_monitor "${args[1]}" "${args[2]}"
fi
